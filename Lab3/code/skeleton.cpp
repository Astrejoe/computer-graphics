#include <iostream>
#include <glm/glm.hpp>
#include <SDL.h>
#include "SDLauxiliary.h"
#include "TestModel.h"
#include <algorithm>
#ifdef EM_BUILD
    #include <emscripten.h>
#endif

using namespace std;
using glm::vec3;
using glm::vec2;
using glm::ivec2;
using glm::mat3;

class Camera
{
public:
    int focalLength;
    glm::vec3 cameraPos;
    float yaw; // in radians

    Camera() : focalLength(500), cameraPos(0, 0, -3.001f), yaw(0.0f) {};

    glm::mat3 getRotationMatrix()
    {
        return glm::mat3(
                cosf(yaw), 0, sinf(yaw),
                0, 1, 0,
                -sinf(yaw), 0, cosf(yaw)
        );
    }

    glm::vec3 getRight()
    {
        const glm::mat3 R = getRotationMatrix();
        return glm::vec3( R[0][0], R[0][1], R[0][2]);
    }

    glm::vec3 getDown()
    {
        const glm::mat3 R = getRotationMatrix();
        return glm::vec3( R[1][0], R[1][1], R[1][2]);
    }

    glm::vec3 getForward()
    {
        const glm::mat3 R = getRotationMatrix();
        return glm::vec3( R[2][0], R[2][1], R[2][2]);
    }
};

struct Pixel
{
    int x;
    int y;
    float zinv;
    vec3 pos3d;
};

struct Vertex
{
    vec3 position;
};

// ----------------------------------------------------------------------------
// GLOBAL VARIABLES

const int SCREEN_WIDTH = 500;
const int SCREEN_HEIGHT = 500;
SDL_Surface* screen;
int t;
vector<Triangle> triangles;
Camera camera;
int focalLength = 500;
vec3 currentColor;
float depthBuffer[SCREEN_HEIGHT][SCREEN_WIDTH];
vec3 lightPos(0, -0.5, -0.7);
vec3 lightPower = 14.1f * vec3( 1, 1, 1);
vec3 indirectLightPowerPerArea = 0.5f * vec3(1, 1, 1);
vec3 currentNormal;
vec3 currentReflectance;
SDL_Event inputEvent;
int counter = 0;

// ----------------------------------------------------------------------------
// FUNCTIONS

void Update();
void Draw();
void VertexShader( const Vertex & v, Pixel & p);
void Interpolate( const Pixel a, const Pixel b, vector<Pixel> & result );
void ComputePolygonRows(const vector<Pixel> & vertexPixels, vector<Pixel> & leftPixels, vector<Pixel> & rightPixels );
void DrawRows( const vector<Pixel> & leftPixels, const vector<Pixel> & rightPixels );
void DrawPolygon( const vector<Vertex> & vertices );
void PixelShader( const Pixel & p );
void iteration();

int main( int argc, char* argv[] )
{
	LoadTestModel( triangles );
	screen = InitializeSDL( SCREEN_WIDTH, SCREEN_HEIGHT );
	t = SDL_GetTicks();	// Set start value for timer.

#ifdef EM_BUILD
    emscripten_set_main_loop(&iteration, 0, true);
#else
    while( NoQuitMessageSDL() )
    {
        iteration();
    }
#endif

	// SDL_SaveBMP( screen, "screenshot.bmp" );
	return 0;
}

#ifdef EM_BUILD
void Update()
{
	// Compute frame time:
	int t2 = SDL_GetTicks();
	float dt = float(t2-t);
	t = t2;
	counter++;
	if(counter > 30) {
	    cout << "Render time: " << dt << " ms, FPS: " << 1000.0f / dt << endl;
	    counter = 0;
	}

    glm::vec3 forward(0.0f, 0.0f, 0.1f);
    glm::vec3 right(0.1f, 0.0f, 0.0f);
    glm::vec3 up(0.0f, -0.1f, 0.0f);

    while( SDL_PollEvent( &inputEvent ) != 0 )
    {
        if (inputEvent.type == SDL_KEYDOWN) {
            switch (inputEvent.key.keysym.sym) {
                case SDLK_UP:
	                if(camera.cameraPos.z <= -3.5f)
		                camera.cameraPos += 0.001f * dt * camera.getForward();
                    break;

                case SDLK_DOWN:
	                camera.cameraPos -= 0.001f * dt * camera.getForward();
                    break;

                case SDLK_LEFT:
		            camera.yaw += 0.0005 * dt;
                    break;

                case SDLK_RIGHT:
		            camera.yaw -= 0.0005 * dt;
                    break;

                case SDLK_j:
                    camera.yaw += 0.1; // in radians
                    break;

                case SDLK_l:
                    camera.yaw -= 0.1; // in radians
                    break;

                case SDLK_w:
		            lightPos += 0.001f * dt * vec3(0, 0, 1);
                    break;

                case SDLK_a:
                    lightPos -= 0.001f * dt * vec3(1, 0, 0);
                    break;

                case SDLK_s:
                    lightPos -= 0.001f * dt * vec3(0, 0, 1);
                    break;

                case SDLK_d:
                    lightPos += 0.001f * dt * vec3(1, 0, 0);
                    break;

                case SDLK_q:
                    lightPos += 0.001f * dt * vec3(0, 1, 0);
                    break;

                case SDLK_e:
                    lightPos -= 0.001f * dt * vec3(0, 1, 0);
                    break;
            }
        }
        else if( inputEvent.type == SDL_KEYUP )
        {
        }
    }
}

#else

void Update()
{
	// Compute frame time:
	int t2 = SDL_GetTicks();
	float dt = float(t2-t);
	t = t2;
    counter++;
    if(counter > 30) {
        cout << "Render time: " << dt << " ms, FPS: " << 1000.0f / dt << endl;
        counter = 0;
    }

	Uint8* keystate = SDL_GetKeyState(0);

	if( keystate[SDLK_UP] )
	    if(camera.cameraPos.z <= -3.5f)
		    camera.cameraPos += 0.001f * dt * camera.getForward();

	if( keystate[SDLK_DOWN] )
	    camera.cameraPos -= 0.001f * dt * camera.getForward();

	if( keystate[SDLK_RIGHT] )
		camera.yaw -= 0.0005 * dt;

	if( keystate[SDLK_LEFT] )
		camera.yaw += 0.0005 * dt;

    if( keystate[SDLK_w] )
		lightPos += 0.001f * dt * vec3(0, 0, 1);

	if( keystate[SDLK_s] )
        lightPos -= 0.001f * dt * vec3(0, 0, 1);

	if( keystate[SDLK_d] )
        lightPos += 0.001f * dt * vec3(1, 0, 0);

	if( keystate[SDLK_a] )
        lightPos -= 0.001f * dt * vec3(1, 0, 0);

	if( keystate[SDLK_e] )
        lightPos -= 0.001f * dt * vec3(0, 1, 0);

	if( keystate[SDLK_q] )
        lightPos += 0.001f * dt * vec3(0, 1, 0);
}
#endif

void Draw()
{
	SDL_FillRect( screen, 0, 0 );
	for( int y=0; y<SCREEN_HEIGHT; ++y)
	    for( int x=0; x<SCREEN_WIDTH; ++x)
	        depthBuffer[y][x] = 0;

	if( SDL_MUSTLOCK(screen) )
		SDL_LockSurface(screen);
	
	for( int i=0; i<triangles.size(); ++i )
	{
	    currentColor = triangles[i].color;
		vector<Vertex> vertices(3);
		vertices[0].position = triangles[i].v0;
		vertices[1].position = triangles[i].v1;
        vertices[2].position = triangles[i].v2;
        currentNormal = triangles[i].normal;
        currentReflectance = triangles[i].color;
        DrawPolygon( vertices );
	}
	
	if ( SDL_MUSTLOCK(screen) )
		SDL_UnlockSurface(screen);

	SDL_UpdateRect( screen, 0, 0, 0, 0 );
}

void DrawPolygon( const vector<Vertex> & vertices )
{
    int V = vertices.size();
    vector<Pixel> vertexPixels(V);
    for( int i=0; i<V; ++i)
        VertexShader( vertices[i], vertexPixels[i]);
    vector<Pixel> leftPixels;
    vector<Pixel> rightPixels;
    ComputePolygonRows( vertexPixels, leftPixels, rightPixels);
    DrawRows(leftPixels, rightPixels);
}

void VertexShader( const Vertex & v, Pixel & p)
{
    Vertex vertexViewSpace = {(v.position - camera.cameraPos) * camera.getRotationMatrix()};
    p.zinv = 1.0f / vertexViewSpace.position.z;
    p.x = (int)(focalLength * vertexViewSpace.position.x * p.zinv + SCREEN_WIDTH / 2);
    p.y = (int)(focalLength * vertexViewSpace.position.y * p.zinv + SCREEN_HEIGHT / 2);
    p.pos3d = v.position * p.zinv;
}

void ComputePolygonRows(const vector<Pixel> & vertexPixels, vector<Pixel> & leftPixels, vector<Pixel> & rightPixels )
{
    // 1. find max and min y-value of the polygon
    // and compute the number of rows it occupies.
    vector<int> yValues(vertexPixels.size());
    for( int i=0; i<vertexPixels.size(); ++i )
    {
        yValues[i] = vertexPixels[i].y;
    }
    auto minmax = std::minmax_element(begin(yValues), end(yValues));
    int min = *minmax.first;
    int max = *minmax.second;
    int numberRows = max - min + 1;


    // 2. Resize leftPixels and rightPixels
    // so that they have an element for each row
    leftPixels.resize(numberRows);
    rightPixels.resize(numberRows);

    // 3. Initialize the x-coordinates in leftPixels
    // to some really large value and the x-coordinates
    // in rightPixels to some really small value
    for (int i=0; i<numberRows; ++i)
    {
        leftPixels[i].x = INT_MAX;
        rightPixels[i].x = INT_MIN;
    }

    // 4. Loop through all edges of the polygon and use
    // linear interpolation to find the x-coordinate for
    // each row it occupies. Update the corresponding
    // values in rightPixels and leftPixels
    for( int i=0; i<vertexPixels.size(); ++i )
    {
        Pixel pixel1 = vertexPixels[i];

        int j = (i+1) % vertexPixels.size(); // the next vertex
        Pixel pixel2 = vertexPixels[j];

        int numberOfRows = std::abs(pixel1.y - pixel2.y) + 1;
        vector<Pixel> line(numberOfRows);
        Interpolate(pixel1, pixel2, line);
        for( Pixel pixel : line)
        {
            int index = pixel.y - min;
            if( pixel.x < leftPixels[index].x)
            {
                leftPixels[index] = pixel;
            }
            if( pixel.x > rightPixels[index].x)
            {
                rightPixels[index] = pixel;
            }
        }
    }
}

void Interpolate( const Pixel a, const Pixel b, vector<Pixel> & result )
{
    int N = result.size();
    float denominator = float(max(N-1, 1));
    float stepX = (b.x-a.x) / denominator;
    float stepY = (b.y-a.y) / denominator;
    float stepZInv = (b.zinv-a.zinv) / denominator;
    vec3 stepPosition = (b.pos3d - a.pos3d) / denominator;
    float currentX = (float) a.x;
    float currentY = (float) a.y;
    float currentZInv = a.zinv;
    vec3 currentIllumination = a.pos3d;
    for( int i=0; i<N; ++i )
    {
        result[i] = {(int)currentX, (int)currentY, currentZInv, currentIllumination};
        currentX += stepX;
        currentY += stepY;
        currentZInv += stepZInv;
        currentIllumination += stepPosition;
    }
}

void DrawRows( const vector<Pixel> & leftPixels, const vector<Pixel> & rightPixels )
{
    for( int i=0; i<leftPixels.size(); i++)
    {
        vector<Pixel> line(rightPixels[i].x - leftPixels[i].x + 1);
        Interpolate(leftPixels[i], rightPixels[i], line);
        for( Pixel pixel : line)
        {
            PixelShader(pixel);
        }
    }
}


void PixelShader( const Pixel & p )
{
    int x = p.x;
    int y = p.y;
    if( x >= 0 && x <= SCREEN_WIDTH && y >= 0 && y <= SCREEN_HEIGHT && p.zinv > depthBuffer[y][x] )
    {
        vec3 fromIToLight = lightPos - p.pos3d / p.zinv;
        glm::vec3 fromIToLightNormalized = glm::normalize(fromIToLight);
        glm::vec3 P = lightPower;
        glm::vec3 rHat = fromIToLightNormalized;
        glm::vec3 nHat = currentNormal;
        float rSquared = glm::dot(fromIToLight, fromIToLight);
        vec3 D = (P * std::max(glm::dot(rHat, nHat), 0.0f) ) / (float)(4 * M_PI * rSquared);
        vec3 R = (D + indirectLightPowerPerArea) * currentReflectance;

        depthBuffer[y][x] = p.zinv;
        PutPixelSDL( screen, x, y, R );
    }
}

void iteration()
{
    Update();
    Draw();
}
