// Introduction lab that covers:
// * C++
// * SDL
// * 2D graphics
// * Plotting pixels
// * Video memory
// * Color representation
// * Linear interpolation
// * glm::vec3 and std::vector

#include "SDL.h"
#include <iostream>
#include <glm/glm.hpp>
#include <vector>
#include "SDLauxiliary.h"

using namespace std;
using glm::vec3;

// --------------------------------------------------------
// GLOBAL VARIABLES

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;
SDL_Surface* screen;

// --------------------------------------------------------
// FUNCTION DECLARATIONS

void Draw();
void Interpolate();
void InterpolateFloat();
void testInterpolate();
void testInterpolateFloat();

// --------------------------------------------------------
// FUNCTION DEFINITIONS

int main( int argc, char* argv[] )
{
	screen = InitializeSDL( SCREEN_WIDTH, SCREEN_HEIGHT );
	testInterpolateFloat();
	testInterpolate();
	while( NoQuitMessageSDL() )
	{
		Draw();
	}
	SDL_SaveBMP( screen, "screenshot.bmp" );
	return 0;
}

void InterpolateFloat( float a, float b, vector<float>& result )
{
	if( result.size() <= 0 )
	{
		// do nothing
	} else if( result.size() == 1 )
	{
		result[0] = (a + b) / 2;
	} else 
	{
		float deltaX = (b - a) / (result.size() - 1);
		for( int i=0; i<result.size(); ++i )
		{
			result[i] = a + i * deltaX;
		}
	}
}

void Interpolate( vec3 a, vec3 b, vector<vec3>& result )
{
	// For each axis, initialize vector
	vector<float> resultX(result.size());
	vector<float> resultY(result.size());
	vector<float> resultZ(result.size());

	// For each axis, interpolate
	InterpolateFloat( a.x, b.x, resultX);
	InterpolateFloat( a.y, b.y, resultY);
	InterpolateFloat( a.z, b.z, resultZ);

	// combine the interpolations
	for( int i=0; i<result.size(); ++i )
	{
		result[i] = vec3(resultX[i], resultY[i], resultZ[i]);
	}
	
}

void testInterpolateFloat()
{
	vector<float> result(10);
	InterpolateFloat(5, 14, result);
	cout << "expected: 5 6 7 8 9 10 11 12 13 14" << endl;
	cout << "computed: ";
	for( int i=0; i<result.size(); ++i)
	{
		cout << result[i] << " ";
	}
	cout << endl;
}

void testInterpolate()
{
	vector<vec3> result( 4 );
	vec3 a(1,4,9.2);
	vec3 b(4,1,9.8);
	Interpolate( a, b, result );
	cout << "expected: ( 1, 4, 9.2 ) ( 2, 3, 9.4 ) ( 3, 2, 9.6 ) ( 4, 1, 9.8 )" << endl;
	cout << "computed: ";
	for( int i=0; i<result.size(); ++i )
	{
		cout << "( "
		<< result[i].x << ", "
		<< result[i].y << ", "
		<< result[i].z << " ) ";
	}
	cout << endl;
}

void Draw()
{
	vec3 topLeft(1,0,0); // red
	vec3 topRight(0,0,1); // blue
	vec3 bottomRight(0,1,0); // green
	vec3 bottomLeft(1,1,0); // yellow

	vector<vec3> leftSide( SCREEN_HEIGHT );
	vector<vec3> rightSide( SCREEN_HEIGHT );

	// vertical interpolation
	Interpolate( topLeft, bottomLeft, leftSide );
	Interpolate( topRight, bottomRight, rightSide );

	for( int y=0; y<SCREEN_HEIGHT; ++y )
	{
		vector<vec3> horizontalInterpolation(SCREEN_WIDTH);
		Interpolate(leftSide[y], rightSide[y], horizontalInterpolation);


		for( int x=0; x<SCREEN_WIDTH; ++x )
		{
			vec3 color = horizontalInterpolation[x];
			PutPixelSDL( screen, x, y, color );
		}
	}

	if( SDL_MUSTLOCK(screen) )
		SDL_UnlockSurface(screen);

	SDL_UpdateRect( screen, 0, 0, 0, 0 );
}
