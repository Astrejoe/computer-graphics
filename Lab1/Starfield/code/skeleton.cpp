// Introduction lab that covers:
// * C++
// * SDL
// * 2D graphics
// * Plotting pixels
// * Video memory
// * Color representation
// * Linear interpolation
// * glm::vec3 and std::vector

#include "SDL.h"
#include <iostream>
#include <glm/glm.hpp>
#include <vector>
#include "SDLauxiliary.h"
#ifdef EM_BUILD
    #include <emscripten.h>
#endif

using namespace std;
using glm::vec3;

// --------------------------------------------------------
// GLOBAL VARIABLES

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;
SDL_Surface* screen;
vector<vec3> stars( 1000 );
int t; // time at last tick
const float V = 0.0003; // velocity
int counter = 0; // used to create back trail
const vec3 BLACK = vec3(0, 0, 0);
#ifdef EM_BUILD
    const int trailConstant = 3;
#else
    const int trailConstant = 45;
#endif



// --------------------------------------------------------
// FUNCTION DECLARATIONS

void Draw();
float randomFloat(float min, float max);
void update();
void InterpolateFloat(float a, float b, vector<float>& result);
void Interpolate( vec3 a, vec3 b, vector<vec3>& result );
void iteration();

// --------------------------------------------------------
// FUNCTION DEFINITIONS

int main( int argc, char* argv[] )
{
	screen = InitializeSDL( SCREEN_WIDTH, SCREEN_HEIGHT );

	// give stars random initial locations
	for( int i=0; i<stars.size(); ++i )
    {
	    float randomX = randomFloat(-1, 1);
        float randomY = randomFloat(-1, 1);
        float randomZ = randomFloat(0, 1);
        vec3 star = vec3(randomX, randomY, randomZ);
        stars[i] = star;
    }

	// set initial t
	t = SDL_GetTicks();
#ifdef EM_BUILD
	emscripten_set_main_loop(&iteration, 0, true);
#else
    while( NoQuitMessageSDL() )
	{
        iteration();
	}
#endif
	// SDL_SaveBMP( screen, "screenshot.bmp" );
	return 0;
}

float randomFloat(float min, float max)
{
    float delta = max - min;
    float randomBetween0And1 = float(rand() / float(RAND_MAX));
    return delta * randomBetween0And1 + min;
}

void update()
{
    int t2 = SDL_GetTicks(); // current time
    float dt = float(t2 - t); // time since last tick
    t = t2; // update time of last tick

    for( int s=0; s<stars.size(); ++s )
    {
        stars[s].z = stars[s].z - V * dt;

        // stars loop around. Thereby giving infinite stars
        if( stars[s].z <= 0 )
            stars[s].z += 1;
        if( stars[s].z > 1 )
            stars[s].z -= 1;
    }
}

void Draw()
{
    if( SDL_MUSTLOCK(screen) )
        SDL_LockSurface(screen);
    counter++;
    if(counter > trailConstant) {
        for( int i = 0; i < SCREEN_WIDTH; i++ ) {
            for( int j = 0; j < SCREEN_HEIGHT; j++ ) {
                PutPixelSDL( screen, i, j, BLACK );
            }
        }
        counter = 0;
    };
    for( size_t s=0; s<stars.size(); ++s )
    {
        // location star
        float f = SCREEN_HEIGHT / 2;
        vec3 star = stars[s];
        float x = f * star.x / star.z + SCREEN_WIDTH / 2;
        float y = f * star.y / star.z + SCREEN_HEIGHT / 2;

        vec3 color = 0.4f * vec3(1, 1, 1) / star.z;
        PutPixelSDL( screen, x, y, color );

    }
    if( SDL_MUSTLOCK(screen) )
        SDL_UnlockSurface(screen);
    SDL_UpdateRect( screen, 0, 0, 0, 0 );
}

void iteration()
{
    update();
    Draw();
}