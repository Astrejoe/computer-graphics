#include <iostream>
#include <glm/glm.hpp>
#include <SDL.h>
#include "SDLauxiliary.h"
#include "TestModel.h"
#include <glm/gtx/string_cast.hpp>
#ifdef EM_BUILD
    #include <emscripten.h>
#endif

using namespace std;
using glm::vec3;
using glm::mat3;

const int SCREEN_WIDTH = 500;
const int SCREEN_HEIGHT = 500;

struct Intersection
{
    vec3 position;
    float distance;
    int triangleIndex;
};

struct Ray
{
    glm::vec3 start;
    glm::vec3 dir;
};

class Camera
{
public:
    float focalLength;
    glm::vec3 cameraPos;
    float yaw; // in radians

    Camera() : focalLength(350), cameraPos(0, 0, -1250.0f / (float)SCREEN_WIDTH), yaw(0.0f) {};

    glm::mat3 getRotationMatrix()
    {
        return glm::mat3(
                cosf(yaw), 0, sinf(yaw),
                0, 1, 0,
                -sinf(yaw), 0, cosf(yaw)
                );
    }

    glm::vec3 getRight()
    {
        const glm::mat3 R = getRotationMatrix();
        return glm::vec3( R[0][0], R[0][1], R[0][2]);
    }

    glm::vec3 getDown()
    {
        const glm::mat3 R = getRotationMatrix();
        return glm::vec3( R[1][0], R[1][1], R[1][2]);
    }

    glm::vec3 getForward()
    {
        const glm::mat3 R = getRotationMatrix();
        return glm::vec3( R[2][0], R[2][1], R[2][2]);
    }
};

class Light
{
public:
    glm::vec3 lightPos;
    glm::vec3 color;
    float brightness;

    Light() : lightPos(0.0f, -0.5f, -0.7f), color(1.0f, 1.0f, 1.0f),  brightness(14.0f) { };

    glm::vec3 getLightColor()
    {
        return brightness * color;
    }
};

// ----------------------------------------------------------------------------
// GLOBAL VARIABLES


const glm::vec3 indirectLight = 0.5f * glm::vec3( 1.0f, 1.0f, 1.0f);
SDL_Surface* screen;
int t;
std::vector<Triangle> triangles;
const float maxFloat = std::numeric_limits<float>::max();
Camera camera;
Light light;
SDL_Event inputEvent;

// ----------------------------------------------------------------------------
// FUNCTIONS

void Update();
void Draw();
bool findIntersection(const Ray & ray, const Triangle triangle, Intersection & intersection);
bool ClosestIntersection(
        const Ray & ray,
        const vector<Triangle> & triangles,
        Intersection & closestIntersection
);
glm::vec3 DirectLight( const Intersection & i);
void iteration();

int main( int argc, char* argv[] )
{
	screen = InitializeSDL( SCREEN_WIDTH, SCREEN_HEIGHT );
	t = SDL_GetTicks();	// Set start value for timer.
	LoadTestModel(triangles);

#ifdef EM_BUILD
    emscripten_set_main_loop(&iteration, 0, true);
#else
    while( NoQuitMessageSDL() )
    {
        iteration();
    }
#endif

	// SDL_SaveBMP( screen, "screenshot.bmp" );
	return 0;
}

#ifdef EM_BUILD
void Update()
{
    // Compute frame time:
    int t2 = SDL_GetTicks();
    float dt = float(t2-t);
    t = t2;
    cout << "Render time: " << dt << " ms, FPS: " << 1000.0f / dt << endl;

    glm::vec3 forward(0.0f, 0.0f, 0.1f);
    glm::vec3 right(0.1f, 0.0f, 0.0f);
    glm::vec3 up(0.0f, -0.1f, 0.0f);

    while( SDL_PollEvent( &inputEvent ) != 0 )
    {
        if (inputEvent.type == SDL_KEYDOWN) {
            switch (inputEvent.key.keysym.sym) {
                case SDLK_UP:
                    camera.cameraPos += camera.getForward();
                    break;

                case SDLK_DOWN:
                    camera.cameraPos -= camera.getForward();
                    break;

                case SDLK_LEFT:
                    camera.cameraPos -= camera.getRight();
                    break;

                case SDLK_RIGHT:
                    camera.cameraPos += camera.getRight();
                    break;

                case SDLK_i:
                    camera.focalLength += 10;
                    break;

                case SDLK_j:
                    camera.yaw += 0.1; // in radians
                    break;

                case SDLK_k:
                    camera.focalLength -= 10;
                    break;

                case SDLK_l:
                    camera.yaw -= 0.1; // in radians
                    break;

                case SDLK_w:
                    light.lightPos += forward;
                    break;

                case SDLK_a:
                    light.lightPos -= right;
                    break;

                case SDLK_s:
                    light.lightPos -= forward;
                    break;

                case SDLK_d:
                    light.lightPos += right;
                    break;

                case SDLK_q:
                    light.lightPos += up;
                    break;

                case SDLK_e:
                    light.lightPos -= up;
                    break;
            }
        }
        else if( inputEvent.type == SDL_KEYUP )
        {
        }
    }
}

#else

void Update()
{
	// Compute frame time:
	int t2 = SDL_GetTicks();
	float dt = float(t2-t);
	t = t2;
    cout << "Render time: " << dt << " ms, FPS: " << 1000.0f / dt << endl;

    const Uint8* keystate = SDL_GetKeyState( NULL );
    if( keystate[SDLK_UP] )
    {
        camera.cameraPos += camera.getForward();
    }
    if( keystate[SDLK_DOWN] )
    {
        camera.cameraPos -= camera.getForward();
    }
    if( keystate[SDLK_LEFT] )
    {
        camera.cameraPos -= camera.getRight();
    }
    if( keystate[SDLK_RIGHT] )
    {
        camera.cameraPos += camera.getRight();

    }
    if( keystate[SDLK_i])
    {
        camera.focalLength += 10;
    }
    if( keystate[SDLK_k])
    {
        camera.focalLength -= 10;
    }
    if( keystate[SDLK_j])
    {
        camera.yaw += 0.1; // in radians
    }
    if( keystate[SDLK_l])
    {
        camera.yaw -= 0.1; // in radians
    }

    glm::vec3 forward(0.0f, 0.0f, 0.1f);
    glm::vec3 right(0.1f, 0.0f, 0.0f);
    glm::vec3 up(0.0f, -0.1f, 0.0f);

    if( keystate[SDLK_w])
    {
        light.lightPos += forward;
    }
    if( keystate[SDLK_s])
    {
        light.lightPos -= forward;
    }
    if( keystate[SDLK_a])
    {
        light.lightPos -= right;
    }
    if( keystate[SDLK_d])
    {
        light.lightPos += right;
    }
    if( keystate[SDLK_q])
    {
        light.lightPos += up;
    }
    if( keystate[SDLK_e])
    {
        light.lightPos -= up;
    }
}
#endif

void Draw()
{
	if( SDL_MUSTLOCK(screen) )
		SDL_LockSurface(screen);

	for( int y=0; y<SCREEN_HEIGHT; ++y )
	{
		for( int x=0; x<SCREEN_WIDTH; ++x )
		{
		    Ray ray;
		    ray.start = camera.cameraPos;
		    // ray.dir = glm::vec3((float) (x - SCREEN_WIDTH / 2), (float) (y - SCREEN_HEIGHT / 2), camera.focalLength);
		    ray.dir = (float) (x - SCREEN_WIDTH / 2) * camera.getRight() + (float) (y - SCREEN_HEIGHT / 2) * camera.getDown() + camera.focalLength * camera.getForward();
		    Intersection intersection;
		    vec3 color;
		    if ( ClosestIntersection(ray, triangles, intersection) )
            {
		        color = triangles[intersection.triangleIndex].color * ( DirectLight(intersection) + indirectLight);
            }
		    else
            {

                color = glm::vec3( 0.0, 0.0, 0.0 );
            }
			PutPixelSDL( screen, x, y, color );
		}
	}

	if( SDL_MUSTLOCK(screen) )
		SDL_UnlockSurface(screen);

	SDL_UpdateRect( screen, 0, 0, 0, 0 );
}
bool findIntersection(const Ray & ray, const Triangle triangle, const int triangleIndex, Intersection & intersection)
{
    vec3 v0 = triangle.v0;
    vec3 v1 = triangle.v1;
    vec3 v2 = triangle.v2;
    vec3 e1 = v1 - v0;
    vec3 e2 = v2 - v0;

    vec3 s = ray.start;
    vec3 d = ray.dir;

    vec3 b = s - v0;
    mat3 A( -d, e1, e2 );
    vec3 x = glm::inverse( A ) * b;

    float t = x.x;
    float u = x.y;
    float v = x.z;
    // 0.001f threshold to ensure that if a ray is draw from a triangle, that triangle itself isn't included in intersection calculation
    if ( t > 0.0001f && u >= 0.0f && v >= 0.0f && u + v <= 1.0f)
    {
        intersection.position = s + t * d;
        intersection.distance = glm::length(t * d);
        intersection.triangleIndex = triangleIndex;
        return true;
    } else {
        return false;
    }

}

bool ClosestIntersection(
        const Ray & ray,
        const vector<Triangle> & triangles,
        Intersection & closestIntersection
)
{
    // initialize closestIntersection with max distance
    closestIntersection.distance = maxFloat;

    // stores the current triangle intersection we're checking.
    Intersection currentIntersection;
    for( int i=0; i<triangles.size(); i++)
    {
        if( findIntersection(ray, triangles[i], i, currentIntersection) && currentIntersection.distance < closestIntersection.distance)
        {
            closestIntersection = currentIntersection;
        }
    }

    return (closestIntersection.distance < maxFloat);
}


glm::vec3 DirectLight( const Intersection & i)
{
    glm::vec3 fromIToLight = light.lightPos - i.position;
    glm::vec3 fromIToLightNormalized = glm::normalize(fromIToLight);
    Ray rayIntersectionLight;
    rayIntersectionLight.start = i.position;
    rayIntersectionLight.dir = fromIToLightNormalized;
    Intersection closestIntersection;
    bool foundIntersection = ClosestIntersection(rayIntersectionLight, triangles, closestIntersection);
    if ( foundIntersection && glm::distance(closestIntersection.position, i.position) < glm::distance(light.lightPos, i.position))
    {
        return glm::vec3(0.0f, 0.0f, 0.0f);
    }
    else
    {
        glm::vec3 P = light.getLightColor();
        glm::vec3 rHat = fromIToLightNormalized;
        glm::vec3 nHat = glm::normalize(triangles[i.triangleIndex].normal);
        float rSquared = glm::dot(fromIToLight, fromIToLight);
        return (P * std::max(glm::dot(rHat, nHat), 0.0f) ) / (float)(4 * M_PI * rSquared);
    }
}

void iteration()
{
    Update();
    Draw();
}