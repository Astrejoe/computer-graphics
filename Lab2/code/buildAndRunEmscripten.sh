#!/bin/bash

cd ./build_emscripten
cmake -DTARGET="Emscripten" ..
cmake --build .
emrun ./raytracing.html