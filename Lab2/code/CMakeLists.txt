# Top-Level CMakeList.txt

cmake_minimum_required (VERSION 2.6)

if(${TARGET} STREQUAL "Emscripten")
	set(CMAKE_TOOLCHAIN_FILE "/home/robert/bin/emsdk/upstream/emscripten/cmake/Modules/Platform/Emscripten.cmake")
endif()
set(CMAKE_BUILD_TYPE "Release")

project ( raytracing )

add_executable( raytracing skeleton.cpp)

if ( ${TARGET} STREQUAL "Emscripten" )
	set(USE_FLAGS "--emrun")
	if( ${CMAKE_BUILD_TYPE} STREQUAL "Debug")
		set(USE_FLAGS "${USE_FLAGS} -s STACK_OVERFLOW_CHECK=1 -s DEMANGLE_SUPPORT=1 -g4 --source-map-base http://localhost:8080/source-map -v -O0")
	elseif( ${CMAKE_BUILD_TYPE} STREQUAL "Release")
		set(USE_FLAGS "${USE_FLAGS} -g0 -O3")
	endif()
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${USE_FLAGS}")
	set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${USE_FLAGS}")
	set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${USE_FLAGS}")

	include_directories(
			${PROJECT_SOURCE_DIR}/glm
	)
	# set(CMAKE_EXECUTABLE_SUFFIX .html)
	target_compile_definitions(raytracing PRIVATE EM_BUILD)
else()
	find_package (SDL)
	include_directories(
			${SDL_INCLUDE_DIR}
			${PROJECT_SOURCE_DIR}/glm
	)
	target_link_libraries(raytracing ${SDL_LIBRARY})
endif()

